# Pure Go Git Library [![GoDoc](https://godoc.org/bitbucket.org/dkolbly/git?status.svg)](https://godoc.org/bitbucket.org/dkolbly/git)


This is a library for accessing Git repositories.  It works
against local repositories.

## Example

```
package main

import (
	"fmt"

	"bitbucket.org/dkolbly/git"
)

func main() {
	g, _ := git.Open(".git")     // open the store
	ptr, _ := g.Branch("master") // find the "master" commit
	x, _ := g.Get(ptr).Load()    // load it from disk by ptr
	commit := x.(*git.Commit)    // it is a commit object
	fmt.Printf("Author: %s\n", commit.Author)
	fmt.Printf("Message: %s\n", commit.Message)
}
```
