package git

import (
	"bufio"
	"bytes"
	"os"
	"path"
)

func (g *GitDir) loadPackedRefs() map[string]string {
	ix := make(map[string]string)

	fd, err := os.Open(path.Join(g.Dir, "packed-refs"))
	if err != nil {
		panic(err)
		return ix
	}
	defer fd.Close()

	src := bufio.NewReader(fd)
	for {
		line, err := src.ReadBytes('\n')
		if err != nil {
			break
		}
		if len(line) > 41 && line[0] != '#' {
			hash := line[0:40]
			name := string(bytes.TrimSpace(line[41:]))
			ix[string(name)] = string(hash)
		}
	}
	return ix
}

func (g *GitDir) getNamedInPackedRefs(t RefType, name string) *NamedRef {
	if g.packedrefs == nil {
		g.packedrefs = g.loadPackedRefs()
	}
	key := path.Join("refs", t.String(), name)
	if target, ok := g.packedrefs[key]; ok {
		if p, ok := ParsePtr(target); ok {
			return &NamedRef{
				Ptr:     p,
				RefType: t,
				Name:    name,
			}
		}
	}
	return nil
}
